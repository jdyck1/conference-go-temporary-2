from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            # if o has the attribute get_api_url
            #    then add its return value to the dictionary
            #    with the key "href"
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()

            #   if the object to decode is the same class as what's in the
            #   model property, then

            #     * create an empty dictionary that will hold the property names
            #       as keys and the property values as values

            #     * for each name in the properties list
            for property in self.properties:
                value = getattr(o, property)

                if property in self.encoders:
                    print(property)
                    # property is location

                    # self.concoders is the value stored at encoders
                    # if property i.e. location is in the empty dictionary encoders
                    encoder = self.encoders[property]
                    # encoder instance = value of element stored at property in the dictionary
                    value = encoder.default(value)
                    # value instance equals the default value of the encoder object. WHY DEFAULT?

                d[property] = value
            #         * get the value of that property from the model instance
            #           given just the property name

            #         * put it into the dictionary with that property name as
            #           the key
            #     * return the dictionary
            return d
        #   otherwise,
        else:
            return super().default(o)
        #       return super().default(o)  # From the documentation
